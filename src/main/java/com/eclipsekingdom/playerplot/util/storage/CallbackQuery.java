package com.eclipsekingdom.playerplot.util.storage;

public interface CallbackQuery<T> {

    void onQueryDone(T t);

}
